#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com> Michele Vidotto <michele.vidotto@uniud.it>
from __future__ import nested_scopes
from optparse import OptionParser
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader, FormatError
from vfork.util import ignore_broken_pipe
from vfork.util import exit, format_usage
from UserDict import UserDict
import re
from pprint import pprint


class Xlator(UserDict):
	""" An all-in-one multiple string substitution class """ 
	def _make_regex(self): 
		""" Build a regular expression object based on the keys of
		the current dictionary """
		return re.compile("(%s)" % "|".join(map(re.escape, self.keys()))) 

	def __call__(self, mo): 
		""" This handler will be invoked for each regex match """
		# Count substitutions
		self.count += 1 # Look-up string
		return self[mo.string[mo.start():mo.end()]]

	def xlat(self, text): 
		""" Translate text, returns the modified text. """ 
		# Reset substitution counter
		self.count = 0 
		# Process text
		return self._make_regex().sub(self, text)



def sobstitute(l, s, fh, iupac):
	idx = [ i for i, ltr in enumerate(s) if ltr in iupac.keys()]
	if idx:
		for j in idx:
			fh.write( '%s\t%i\t%i\t%s\t%s\n' %(l, j-1, j, s[j], iupac[s[j]]) )
	xlat = Xlator(iupac)
	return xlat.xlat(s)
	#print "Changed %d thing(s)" % xlat.count


def main():

	IUPAC = {
		'R' : 'A', # 'R' : ['A','G'],
		'Y' : 'T', # 'Y' : ['C','T'],
		'S' : 'G', # 'S' : ['G','C'],
		'W' : 'T', # 'W' : ['A','T'],
		'K' : 'G', # 'K' : ['G','T'],
		'M' : 'C', # 'M' : ['A','C'],
		'B' : 'G', # 'B' : ['C','G','T'],
		'D' : 'G', # 'D' : ['A','G','T'],
		'H' : 'C', # 'H' : ['A','C','T'],
		'V' : 'G' # 'V' : ['A','C','G']
	}

	parser = OptionParser(usage=format_usage('''
	Usage: %prog [OPTIONS] <FASTA >FASTA

	Converts one or more characters in a FASTA file with the corresponding,
	defined by a python dictionary.
	
	The dictionary con be given by command line.
	Changed positions are listed in a file in bed format.
	'''))
	parser.add_option('-e', '--allow-empty', dest='allow_empty', default=False, action='store_true', help='allow empty sequences')
	parser.add_option('-m', '--multi-line', dest='multi', default=False, action='store_true', help='keep sequences split over multiple lines, each one prefixed by the sequence label')
	parser.add_option('-f', '--bed-file', dest='bedfile', default="changed.bed", help='bed format file that stores changed positions')
	parser.add_option('-d', '--dict-file', dest='dictfile', default="", help='python dictionary file defining IUPAC sobstitutions. I.e.: %s' %IUPAC)
	options, args = parser.parse_args()
	if len(args) != 0:
		exit('Unexpected argument number.')

	if options.dictfile:
		IUPAC = eval(open(options.dictfile, 'r').read())

	with open(options.bedfile, 'w') as fh:

		try:
			for label, seq in MultipleBlockStreamingReader(stdin, join_lines=not options.multi):
				if options.multi:
					seq = list(seq)

				empty_seq = len(seq) == 0
				if empty_seq:
					if not options.allow_empty:
						exit('Empty FASTA sequence in input: ' + label)
					else:
						print ">%s" % label
				else:
					if options.multi:
						for s in seq:
							nseq = sobstitute(label, s, fh, IUPAC)
							print '>%s\n%s' % (label, s)
					else:
						nseq = sobstitute(label, seq, fh, IUPAC)
						print ( '>%s\n%s' % (label, nseq))

		except FormatError as e:
			fh.close()
			exit('Malformed FASTA input: ' + e.args[0])

	fh.close()


if __name__ == '__main__':
	ignore_broken_pipe(main)
