# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>


# TODO:


context prj/rnaseq_assembly

# remove count for kmers with frequence less then indicated
LCOUNT	?= 3
# kmers length
KMERS	?= 16 17 18 19
# fastq sequences, gz compressed
SEQ_GZ	?=
# estimated genome size
G_SIZE ?= 400879909
# define the hash size for jellyfish #9293715826
HASH_SIZE ?= 
# execute random sobstitution of N in sequences
RANDOM_SUB_N ?= FALSE

# recall bmake
MAKE := bmake


# generate links
# each file in SEQ_GZ is linked to a file, which name is a number starting from 0.
# example: 0.fastq.gz 1.fastq.gz ..
# the variable SEQ_GZ_LN is set up
link.mk:
	FILE_LST=""; \
	COUNTER=0; \
	for FILE in $(SEQ_GZ); do \
	  ln -sf $$FILE $$COUNTER.fastq.gz; \
	  FILE_LST="$$FILE_LST $$COUNTER.fastq.gz"; \
	  COUNTER=$$[$$COUNTER +1]; \
	done; \
	printf "SEQ_GZ_LN := $$FILE_LST" >$@

include link.mk


SEQ_FASTQ_N = $(SEQ_GZ_LN)



ifeq ($(RANDOM_SUB_N),TRUE)
# gunzip, SEQ_GZ_LN is given by supra-makefile
SEQ_FASTQ = $(SEQ_GZ_LN:.fastq.gz=.fastq)
%.fastq: %.fastq.gz
	zcat <$< >$@

# 1. Remove read IDs to save memory
# Output for each read files a temp file
SEQ_FASTQ_N = $(SEQ_FASTQ:.fastq=.fastq.N)
%.fastq.N: %.fastq
	@echo
	@echo "++ Step 1: Replacing Ns ... and stripping off read IDs"
	@echo
	$(call module_loader); \
	random_sub_N $^,$@;
endif



# Prepare directories and generate directories list
# setting up the variable DIR_LST
dir_lst.mk: makefile
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_1.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
	  printf "DIR_LST :=" >$@; \
	  for KMER in $(KMERS); do \
	    mkdir -p $$KMER; \   * create dir for given kmer*
	    cd $$KMER; \
	    ln -sf ../$<; \   * link makefile *
	    ln -sf $$RULES_PATH rules.mk; \   * link rules *
	    for FILE in $(SEQ_FASTQ_N); do \   * link .fastq.gz files *
		ln -sf ../$$FILE; \
	    done; \
	    cd ..; \
	    printf " $$KMER" >>$@; \   * add dir name to list *
	  done; \
	fi

include dir_lst.mk


# prevent KMERS variable from being exported
unexport KMERS
# export SEQ_GZ_LN to sub-makefiles so they can know about the list
# of input files in their subdirectory

# execute bmake on each subdirectory
subsystem.flag: dir_lst.mk $(SEQ_FASTQ_N)
	@echo Looking into subdirs: $(MAKE)
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) KMER=$$D && \   * initialize KMER varible of sub-makefile with appropriate value *
	    cd ..; \
	  fi; \
	done; \
	touch $@


.PHONY: test
test:
	@echo $(SEQ_GZ)






ALL +=	link.mk \
	dir_lst.mk \
	$(SEQ_FASTQ) \
	$(SEQ_FASTQ_N) \
	$(DIR_LST) \
	subsystem.flag

INTERMEDIATE += 

CLEAN += $(wildcard *.fastq.gz)

# add dipendence to clean targhet
clean: clean_dir

# recall bmake clean into each subdirectory
.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done
