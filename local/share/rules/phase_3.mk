# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>

# TODO:

.ONESHELL:

extern ../phase_1/subsystem.flag as SUBSYSTEM_FLAG

dir_lst.mk: $(SUBSYSTEM_FLAG)
ifeq ($(wildcard ../phase_1/dir_lst.mk),)
	touch $@
else
	ln -sf ../phase_1/dir_lst.mk $@
endif

include dir_lst.mk


jf_merged_%: dir_lst.mk
	@echo "Install links..."
	for D in $(addprefix ../phase_1/,$(DIR_LST)); do \
	  if [ -d $$D ]; then \
	    ln -sf "$$D/jf_merged_$$(basename $$D)" .; \
	  fi; \
	done

# force conversion of IUPAC charachters to a precise DNA base
# defined within the script. Save the converted bases to a bed file
ref.noIUPAC.fasta:
	zcat <$(REFERENCE) \
	| resolve_iupac -f $(basename $@).bed >$@

# report the list of onverted bases line by line
ref.noIUPAC.bed.gz: ref.noIUPAC.fasta
	gzip -c -9 <$(basename $@) >$@

# use kat sect to produce a fasta style representation of the input sequence file containing K-mer coverage counts 
# mapped across each sequence. K-mer coverage is determined from the provided jellyfish hash
# In addition, a space separated table file containing the mean coverage score and GC of each sequence is produced.
# The row order is identical to the original sequence file.

# NOTE: K-mers containing any Ns derived from sequences in the sequence file not be included.
# Each position of the input sequence file receive a kmer coverage until the N-K+1 position
REF_K_STAT = $(addsuffix -stats.csv,$(addprefix ref.noIUPAC.fasta.k,$(DIR_LST)))
ref.noIUPAC.fasta.k%-stats.csv: ref.noIUPAC.fasta jf_merged_%
	!threads
	$(call module_loader); \
	kat sect \
	--threads=$$THREADNUM \
	--output_prefix=ref.noIUPAC.fasta.k$* \
	--verbose \
	$< \
	jf_merged_$*


# use the script /bioinfotree/binary/appliedgenomics/local/stow/jellyfish-2.2.4/examples/query_per_sequence/query_per_sequence
# to retrieve kmer coverage of at each position of the new IUPAC sobstituted fasta
# with respect to kat sect, the coverage is not returned for bases considered invalid and until the N-K+1 position
REF_K = $(addsuffix .fasta.gz,$(addprefix ref.noIUPAC.k,$(DIR_LST)))
ref.noIUPAC.k%.fasta.gz: ref.noIUPAC.fasta jf_merged_%
	$(call module_loader); \
	query_per_sequence jf_merged_$* $< \
	| gzip -c -9 >$@

REF_K_COUNT = $(addsuffix -counts.fasta.gz,$(addprefix ref.noIUPAC.k,$(DIR_LST)))
ref.noIUPAC.k%-counts.fasta.gz: ref.noIUPAC.fasta.k%-stats.csv
	if [ -s ref.noIUPAC.fasta.k$*-counts.cvg ]; then \
		gzip -c9 <ref.noIUPAC.fasta.k$*-counts.cvg >ref.noIUPAC.k$*-counts.fasta.gz; \
	fi && \
	rm ref.noIUPAC.fasta.k$*-counts.cvg


# generate bigWig from kmer frequencies in fasta format
# fixedStep chrom=chr1 start=1 step=1
ref.noIUPAC.k%.bw: ref.noIUPAC.k%-counts.fasta.gz ref.noIUPAC.fasta
	module purge;
	module load lang/python/2.7;
	module load tools/ucsc-utils/16-Dec-2013;
	wigToBigWig \
	<(zcat $< \
	| fasta2tab \
	| bawk '!/^[\#+,$$]/ { \
	split($$2,a," "); \
	print "fixedStep chrom="$$1" start=1 step=1"; \
	for( i=1; i<=length(a); i++ ) { print a[i] } \
	}') \
	<(fasta_length <$^2) \
	$@


KFREQ_GC_STRAT = $(addsuffix .stratified.boxplot.pdf,$(addprefix meankfreq.vs.gc.k,$(DIR_LST)))
meankfreq.vs.gc.k%.stratified.boxplot.pdf: ref.noIUPAC.fasta.k%-stats.csv
	stratified_boxplot \
	--output-file=$@ \
	--num-bins=20 \
	--xlabel="kmer freq." \
	--ylabel="GC%" \
	--title="$(call first,$(call split,_,$(PRJ))) GC% stratified by $*-mer frequency" \
	--row-names \
	3 4 \
	<$<

LEN_KFREQ_SCATTER = $(addsuffix .scatterplot.pdf,$(addprefix length.vs.kmerfreq.k,$(DIR_LST)))
length.vs.kmerfreq.k%.scatterplot.pdf: ref.noIUPAC.fasta.k%-stats.csv
	scatterplot \
	--remove-na \
	--title="$(call first,$(call split,_,$(PRJ))) sequence length Vs $*-mer frequency" \
	--xlab="kmer frequency" \
	--ylab="sequence length" \
	--pch="1" \
	--output-file=$@ \
	--color="blue" \
	3,5 \
	<$<




.PHONY: test
test:
	@echo $(LEN_KFREQ_SCATTER)


ALL += dir_lst.mk \
	ref.noIUPAC.bed.gz \
	$(REF_K_COUNT) \
	$(REF_K_STAT) \
	$(KFREQ_GC_STRAT) \
	$(LEN_KFREQ_SCATTER) \
	# $(REF_K) \

INTERMEDIATE += ref.noIUPAC.fasta

CLEAN += $(REF_K_COUNT) \
	ref.noIUPAC.bed
