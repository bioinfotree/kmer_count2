# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>


# TODO:


context prj/rnaseq_assembly

# genome size (used for calculating the hash size)
G_SIZE ?= 487000000
# coverage (used for calculating the hash size)
COV ?= 50
# kmers length; passed by supra-makefile
KMER ?= 16
# fastq sequences, gz compressed
SEQ_GZ ?=
# define the hash size for jellyfish #9293715826
HASH_SIZE ?= 
# execute random sobstitution of N in sequences
RANDOM_SUB_N ?= FALSE


tmp:
	mkdir -p $@ || { echo "Failed to create temp dir"; exit 1;}


SEQ_FASTQ_N = $(wildcard *.fastq.gz)

# if not defined HASH_SIZE is calculated this way:
# G + Gcek. with an estimated genome size of G and a coverage of c, the number of expected k-mers is  This assume
# G = estimated genome size
# k = kmer length
# c = coverage
# e = sequencing error rate
hash_size.mk: $(SEQ_FASTQ_N)
ifndef HASH_SIZE
	HASH_SIZE=$$(echo "( $(G_SIZE) + $(G_SIZE) * $(KMER) * $(COV) ) / 100" | bc); \
	echo $$HASH_SIZE; \
	printf "HASH_SIZE := %i" $$HASH_SIZE >$@
else
	printf "HASH_SIZE := $(HASH_SIZE)" >$@
endif

include hash_size.mk



ifeq ($(RANDOM_SUB_N),TRUE)
SEQ_FASTQ_N = $(wildcard *.fastq.N)
endif



ifeq ($(RANDOM_SUB_N),TRUE)
# run JELLYFISH to count kmers
jellyfish.flag: tmp $(SEQ_FASTQ_N)
	!threads
	@echo
	@echo "++ Step 2: Running JELLYFISH to count kmers ..."
	@echo
	$(call module_loader); \
	cat $(call rest,$^) \   * return a list with the first element removed *
	| jellyfish count \
	-m $(KMER) \
	-t $$THREADNUM \
	-o $</jf_tmp \
	$(call jellyfish_options) \
	/dev/fd/0 \
	&& touch $@
else
jellyfish.flag: tmp $(SEQ_FASTQ_N)
	!threads
	@echo
	@echo "++ Step 2: Running JELLYFISH to count kmers ..."
	@echo
	$(call module_loader); \
	zcat $(call rest,$^) \   * return a list with the first element removed *
	| jellyfish count \
	-m $(KMER) \
	-t $$THREADNUM \
	-o $</jf_tmp \
	$(call jellyfish_options) \
	/dev/fd/0 \
	&& touch $@
endif




# merge jellyfish  databases
jf_merged_$(KMER): tmp jellyfish.flag
	@echo
	@echo "++ Step 3: Running JELLYFISH to merge count kmers ..."
	@echo
	if [ -s $</jf_tmp ]; then \
	N_TMP=$$(ls -1 $</jf_tmp* | wc -l); \
	if [ $$N_TMP -eq 1 ]; then \
	mv $</jf_tmp $@; \
	else \
	$(call module_loader); \
	jellyfish merge $</jf_tmp* -o $@; \
	fi; \
	fi



# data for plot histogram
jellyfish.histo: jf_merged_$(KMER)
	!threads
	@echo
	@echo "++ Step 4: Running JELLYFISH to create hostogram of k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish histo \
	-o $@ \
	-t $$THREADNUM \
	$^

# columns in kmer spectrum files jellyfish.histo.count
.META: jellyfish.histo.count
	1	kmer_frequency
	2	num_distinct_kmers
	3	frac_distinct_kmers = (2)/num_distinct_kmers_total
	4	cummulative(2)
	5	cummulative(3) = (4)/num_distinct_kmers_total
	6	num_kmers = (1)*(2)
	7	frac_kmers = (6)/num_kmers_total
	8	cummulative(6)
	9	cummulative(7) = (8)/num_kmers_total

jellyfish.histo.count: jellyfish.histo
	TOTAL_NUM_DISTINCT_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$2; } END {print tot;}' <$<); \
	TOTAL_NUM_KMERS=$$(awk 'BEGIN{ FS=" "; OFS="\t"; tot=0;} !/^[$$,\#+]/ { tot += $$1*$$2; } END {print tot;}' <$<); \
	awk -v total_num_distinct_kmers=$$TOTAL_NUM_DISTINCT_KMERS \
	    -v total_num_kmers=$$TOTAL_NUM_KMERS \
	'BEGIN{ printf "# 1:kmer_frequency\t2:num_distinct_kmers\t3:2/total_num_distinct_kmers\t4:cummulative(2)\t5:4/total_num_distinct_kmers\t6:num_kmers (1)*(2)\t7:6/total_num_kmers\t8:cummulative(6)\t9:8/total_num_kmers\n"; \
		_4=0; \
	        _8=0; \
	        FS=" "; \
	        OFS="\t"; } \
	!/^[$$,\#+]/ \
	{ kmer_frequency = $$1; \
	  num_distinct_kmers = $$2; \
	  _3 = num_distinct_kmers / total_num_distinct_kmers; \
	  _4 += num_distinct_kmers; \
	  _5 = _4 / total_num_distinct_kmers; \
	  _6 = kmer_frequency * num_distinct_kmers; \
	  _7 = _6 / total_num_kmers; \
	  _8 += _6; \
	  _9 = _8 / total_num_kmers; \
	  print kmer_frequency, num_distinct_kmers, _3, _4, _5, _6, _7, _8, _9; \
	}' <$< >$@


# more complete statistics
jellyfish.verbose: jf_merged_$(KMER)
	!threads
	@echo
	@echo "++ Step 5: Running JELLYFISH to create statistics of k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish stats \
	-o $@ \
	$<


# dump lower counts (needed for seecer)
counts_$(KMER)_$(MIN_OCC): jf_merged_$(KMER)
	@echo
	@echo "++ Step 6: Running JELLYFISH to dump k-mer occurence ..."
	@echo
	$(call module_loader); \
	jellyfish dump --lower-count=$(MIN_OCC) -c $< -o $@;

.PHONY: test
test:
	@echo $(wildcard *.fastq.N)



ALL +=	$(SEQ_FASTQ_N) \
	jf_merged_$(KMER) \
	jellyfish.flag \
	jellyfish.histo \
	jellyfish.histo.count \
	jellyfish.verbose

INTERMEDIATE += 

CLEAN +=  tmp
